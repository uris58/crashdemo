#include "AgentAPI.h"
#include "jvmti.h"

static jvmtiEnv *jvmti;

static void addRemoveFieldWatch(jvmtiEnv *jvmti, JNIEnv *jni_env, jclass klass, jfieldID field, bool enable)
{
	jvmtiError err;
	if (enable)
	{
		err = jvmti->SetFieldModificationWatch(klass, field);
		if (err != JVMTI_ERROR_NONE && err != JVMTI_ERROR_DUPLICATE)
		{
			printf("%s\tfailed to set modification watch: %d\n", __FUNCTION__, err);
			return;
		}
		err = jvmti->SetFieldAccessWatch(klass, field);
		if (err != JVMTI_ERROR_NONE && err != JVMTI_ERROR_DUPLICATE)
		{
			printf("%s\tfailed to set access watch: %d\n", __FUNCTION__, err);
			return;
		}
	}
	else
	{
		err = jvmti->ClearFieldModificationWatch(klass, field);
		if (err != JVMTI_ERROR_NONE && err != JVMTI_ERROR_NOT_FOUND)
		{
			printf("%s\tfailed to clear modification watch: %d\n", __FUNCTION__, err);
			return;
		}
		err = jvmti->ClearFieldAccessWatch(klass, field);
		if (err != JVMTI_ERROR_NONE && err != JVMTI_ERROR_NOT_FOUND)
		{
			printf("%s\tfailed to clear access watch: %d\n", __FUNCTION__, err);
			return;
		}
	}
}

JNIEXPORT void JNICALL Java_MyAgent_setWatch(JNIEnv *jni_env, jclass, jboolean enable)
{
	auto klass = jni_env->FindClass("Ljava/net/SocketImpl;");
	if (klass == nullptr)
	{
		printf("cannot find socket class\n");
		return;
	}

	auto fdField = jni_env->GetFieldID(klass, "fd", "Ljava/io/FileDescriptor;");
	if (fdField == nullptr)
	{
		printf("cannot find socket fd field\n");
		return;
	}
	addRemoveFieldWatch(jvmti, jni_env, klass, fdField, enable);
}

static void JNICALL onFieldModified(jvmtiEnv *jvmti_env,
									JNIEnv *jni_env,
									jthread thread,
									jmethodID method,
									jlocation location,
									jclass field_klass,
									jobject parentObject,
									jfieldID field,
									char signature_type,
									jvalue new_value)
{
}

static void JNICALL onFieldAccess(jvmtiEnv *jvmti_env,
								  JNIEnv *jni_env,
								  jthread thread,
								  jmethodID method,
								  jlocation location,
								  jclass field_klass,
								  jobject parentObject,
								  jfieldID field)
{
}

JNIEXPORT jint JNICALL Agent_OnLoad(JavaVM *jvm, char *options, void *reserved)
{
	jvmtiEventCallbacks callbacks = {};
	jvmtiError error;

	jint result = jvm->GetEnv((void **)&jvmti, JVMTI_VERSION_1_1);
	if (result != JNI_OK)
	{
		printf("%s\tUnable to access JVMTI!: %d\n", __FUNCTION__, result);
	}
	else
	{
		printf("Agent succesfully loaded\n");
	}

	jvmtiCapabilities capa = {};

	capa.can_generate_field_modification_events = 1;
	capa.can_generate_field_access_events = 1;
	error = jvmti->AddCapabilities(&capa);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set capabilities: %d\n", __FUNCTION__, error);
	}

	callbacks.FieldModification = &onFieldModified;
	callbacks.FieldAccess = &onFieldAccess;

	error = jvmti->SetEventCallbacks(&callbacks, sizeof(callbacks));
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set event callbacks: %d\n", __FUNCTION__, error);
	}

	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_FIELD_ACCESS, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set event notifications: %d\n", __FUNCTION__, error);
		return JNI_ERR;
	}

	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_FIELD_MODIFICATION, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set event notifications: %d\n", __FUNCTION__, error);
		return JNI_ERR;
	}

	return JNI_OK;
}

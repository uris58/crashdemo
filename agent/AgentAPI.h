#include <jni.h>

#ifndef AgentAPI_H
#define AgentAPI_H
#ifdef __cplusplus
extern "C"
{
#endif

	JNIEXPORT void JNICALL Java_MyAgent_setWatch(JNIEnv *, jclass, jboolean);

#ifdef __cplusplus
}
#endif
#endif

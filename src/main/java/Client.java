import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private Socket conn;

    public Client(int port) throws IOException {
        conn = new Socket("localhost", port);
    }

    public void close() throws IOException {
        conn.close();
    }

    public void ioOnce() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        PrintWriter out = new PrintWriter(conn.getOutputStream(), true);
        out.println("Hello");
        in.readLine();
    }
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private ServerSocket sock;

    public Server() throws IOException {
        sock = new ServerSocket(0);
    }

    public int getPort() {
        return sock.getLocalPort();
    }

    public void start() {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    acceptAndHandle();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();
    }

    private void acceptAndHandle() throws IOException {
        Socket conn = sock.accept();
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        PrintWriter out = new PrintWriter(conn.getOutputStream(), true);
        while (true) {
            String text = in.readLine();
            if (text == null) {
                break;
            }
            out.println("Got " + text);
        }
        conn.close();
    }
}

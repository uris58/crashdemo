import java.io.IOException;

public class Hello {
    public static void main(String[] args) throws IOException, InterruptedException {
        Server s = new Server();
        int port = s.getPort();
        s.start();

        System.out.println("Warmup...");
        for (int i = 0; i < 20000; i++) {
            Client c = new Client(port);
            c.ioOnce();
            c.close();
        }

        System.out.println("Set watch...");
        MyAgent.setWatch(true);
        Thread.sleep(1000,0);

        System.out.println("Again...");
        for (int i = 0; i < 10; i++) {
            Client c = new Client(port);
            c.ioOnce();
            c.close();
        }
        System.out.println("Done");
        System.exit(0);
    }
}

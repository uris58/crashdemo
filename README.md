#Watch Issue demo

To build and run on Ubuntu-based build container:

```bash
apt-get install gradle g++ cmake
cd crashdemo
mkdir build
cd build
cmake ../agent
make
cd ..
JAVA_HOME=/path/to/jdk gradle run
```

It's also possible to do a `gradle jar` and then run the app using:

```bash
/path/to/jdk/bin/java -agentpath:build/libNativeAgent.so -jar build/libs/crashdemo-1.0.0-SNAPSHOT.jar
```

